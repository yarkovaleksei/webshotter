var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

plugins.del = require('del'); // Очиститель директорий
plugins.csso = require('gulp-csso'); // Подчищаем CSS
plugins.less = require('gulp-less'); // Компилирует less -> css
plugins.uglify = require('gulp-uglify'); // Минифицирует js скрипты
plugins.jade = require('gulp-pug'); // Компилирует jade -> html
plugins.concat = require('gulp-concat'); // Объединяет несколько файлов в один
plugins.imagemin = require('gulp-imagemin'); // Минификация изображений
plugins.livereload = require('gulp-livereload'); // При изменении файлов в папке проекта перезагружает страницу в браузере. Требует установленного расширения.
plugins.autoprefixer = require('gulp-autoprefixer'); // Обрабатывает CSS файлы и добавляет не вендорные префиксы некоторым свойствам
plugins.html_prettify = require('gulp-html-prettify'); // Добавляем табуляцию к HTML
plugins.runSequence = require('run-sequence'); // Выполняет задачи в строгой последовательности
plugins.rename = require('gulp-rename'); // Сохраняем выходной файл с нужным именем
plugins.data = require('gulp-data'); // Позволяет подключать файлы с настройками (vars.json и т.п.)

// В настройках указываем пути к нашим папкам и маски для поиска файлов
var options = require('./gulp/options');

function getTask(task) {
	return require('./gulp/gulp-tasks/' + task)(gulp, plugins, options);
}

// Компилирует JADE в HTML
gulp.task('compile_jade', getTask('compile_jade'));
// Сжимает оригиналы картинок
gulp.task('image_min', getTask('image_min'));
// Сжимает и объединяет скрипт приложения из исходников
gulp.task('minify_js', getTask('minify_js'));
// Компилирует LESS в CSS и объединяет стили приложения из исходников
gulp.task('compile_less', getTask('compile_less'));


// Запускает сборку проекта, выполняя остальные задачи в нужном порядке для гарантированно успешной сборки
gulp.task('build', function(callback) {
	plugins.del([options.dest])
		.then(function(paths) {
			plugins.runSequence(['compile_jade',
				'image_min',
				'minify_js',
				'compile_less'
			], callback);
		});
});

// Запускает слежку за файлами в папке src и немедленное выполнение задач при их изменении
gulp.task('watch', function() {
	plugins.livereload.listen();
	gulp
		.watch([
			[options.input.cssdir, options.mask.less_mask].join('/')
		], ['compile_less'])
		.on('change', plugins.livereload.changed);
	gulp
		.watch([options.input.jsdir, options.mask.js_mask].join('/'), ['minify_js'])
		.on('change', plugins.livereload.changed);
	gulp
		.watch([
			[options.input.jadedir, options.mask.jade_mask].join('/'),
			options.input.jadevars
		], ['compile_jade'])
		.on('change', plugins.livereload.changed);
});


// Запускает задачу слежки watch
gulp.task('default', ['watch']);
