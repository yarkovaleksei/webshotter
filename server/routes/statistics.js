var express = require('express');
var router = express.Router();
// Подключаем модель Statistic
var Statistic = require('../models/statistic');


/* GET statistic json */
router.get('/', function(req, res, next) {
	Statistic
		.find({})
		.sort({
			"inc_counter": -1
		})
		.limit(10)
		.exec(function(err, docs) {
			if (err) {
				return res.status(200).json([]);
			} else if (docs) {
				var result = [];
				[].forEach.call(docs, function(doc) {
					result.push({
						"url": doc.url,
						"count": doc.inc_counter
					});
				});
				return res.status(200).json(result);
			} else {
				return res.status(200).json([]);
			}
		});
});

module.exports = router;
