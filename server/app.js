var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/* http://expressjs.com/ru/advanced/best-practice-security.html */
var helmet = require('helmet');
/* https://www.npmjs.com/package/cors */
var cors = require('cors');
/* https://www.npmjs.com/package/compression */
var compression = require('compression');

// модуль для работы с MongoDB
var mongoose = require('mongoose');

// Подсоединяемся к БД
var db_conn_string = 'mongodb://localhost:27017/webshotter';
mongoose.connect(db_conn_string);

var statistics = require('./routes/statistics');
var screenshot = require('./routes/screenshot');


var app = express();

// Включаем определение IP за прокси
app.enable('trust proxy');
// Отключаем заголовок "X-Powered-By"
app.disable('x-powered-by');
// Включаем настройки безопасности сервера
app.use(helmet());
// Включаем CORS
app.use(cors());
// Включаем сжатие данных
app.use(compression());

app.use(favicon(path.join(__dirname, '../public/img', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, '../public')));
// Определяем путь к папке bower_components
app.use('/bower_components',  express.static(path.join(__dirname, '../bower_components')));

app.use('/statistics', statistics);
app.use('/screenshot', screenshot);

// catch 404 and forward to error handler
app.use('*', function(req, res, next) {
	return res.status(404).json({
		message: "404 Not found"
	});
});

module.exports = app;
