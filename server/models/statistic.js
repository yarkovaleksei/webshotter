'use strict';

// модуль для работы с MongoDB
var mongoose = require('mongoose');

// типы полей
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var StatisticSchema = new Schema({
	// ссылка на страницу для съемки
	url: {
		type: String,
		required: true,
		unique: false
	},
	// дата и время создания снимка
	createdate: {
		type: Date,
		default: Date.now
	},
	// IP адрес, с которого был запрос
	ip: {
		type: String,
		required: true,
		unique: false
	},
	// счетчик, если такой IP уже запрашивал такой URL
	inc_counter: {
		type: Number,
		required: false,
		default: 1,
		unique: false
	}
});



module.exports = mongoose.model('Statistic', StatisticSchema);
