var webshot = require('webshot');
var fs = require('fs');
var path = require('path');
var uuid = require('node-uuid');

// Подключаем модель Statistic
var Statistic = require('../models/statistic');

function getDefaultOptions() {
	var __options = {
		// задержка в милисекундах (1000 = 1 секунда) после рендера страницы, чтобы отработали все скрипты
		renderDelay: 0,
		// размер окна браузера
		windowSize: {
			width: 1024,
			height: 768
		},
		// UserAgent браузера для снимка
		userAgent: "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36",
		// размер скриншота
		// если указать, например, height: 'all', то сохранится вся страница по высоте
		shotSize: {
			width: 'window',
			height: 'all'
		},
		// отступы от края окна браузера
		shotOffset: {
			left: 0,
			right: 0,
			top: 0,
			bottom: 0
		},
		// Куки, например, для авторизации и показа закрытых страниц
		cookies: [],
		// Заголовки HTTP
		customHeaders: null,
		phantomConfig: {
			"ssl-protocol": "any",
			'ignore-ssl-errors': 'yes'
		},
		// Если true, то фон скриншота всегда белый
		defaultWhiteBackground: false,
		// CSS стили для страницы
		customCSS: "",
		// Качество картинки, если выбран формат jpg или jpeg
		quality: 75,
		// Формат скриншота: png, jpg, jpeg или pdf
		streamType: ["jpg", "jpeg", "png", "pdf"],
		// Тип источника для сохранения: url(ссылка на страницу), file(путь к локальному файлу) или html(html строка для рендеринга)
		siteType: "url"
	};

	return __options;
}

function merge_options(obj1, obj2) {
	var obj = {};
	for (var attrname in obj1) {
		obj[attrname] = obj1[attrname];
	}
	for (var attrname in obj2) {
		obj[attrname] = obj2[attrname];
	}
	return obj;
}

function url_prepare(url) {
	return url.replace(/^((https?|ftp)\:\/\/)/g, "$1_").replace(/(\/|\:|\-|\s)/g, "_");
}

module.exports = function(req, res, next) {
	var url = null;
	var reqOptions = null;

	if (req.method === "GET") {
		url = req.query.url || null;
		reqOptions = req.query;
	} else {
		return res.status(403).json({
			error: true,
			message: "Method must by GET!"
		});
	}

	if (!url || typeof url !== 'string') {
		return res.status(403).json({
			error: true,
			message: "URL must by string!"
		});
	}

	reqOptions = reqOptions || {};
	var _options = getDefaultOptions();

	// Задержка скрина после рендера страницы
	var _d = reqOptions.d;
	_options.renderDelay = (!!_d && parseInt(_d) > 0) ? _d : _options.renderDelay;

	// Размер окна браузера
	var _ww = reqOptions.ww;
	var _wh = reqOptions.wh;
	_options.windowSize.width = (!!_ww && (_ww === "window" || _ww === "all" || parseInt(_ww) > 0)) ? _ww : _options.windowSize.width;
	_options.windowSize.height = (!!_wh && (_wh === "window" || _wh === "all" || parseInt(_wh) > 0)) ? _wh : _options.windowSize.height;

	// Размер скриншота
	var _sw = reqOptions.sw;
	var _sh = reqOptions.sh;
	_options.shotSize.width = (!!_sw && (_sw === "window" || _sw === "all" || parseInt(_sw) > 0)) ? _sw : _options.shotSize.width;
	_options.shotSize.height = (!!_sh && (_sh === "window" || _sh === "all" || parseInt(_sh) > 0)) ? _sh : _options.shotSize.height;

	// Качество картинки
	var _q = reqOptions.q;
	_options.quality = (!!_q && parseInt(_q) > 0) ? _q : _options.quality;

	// Формат скриншота: png, jpg, jpeg или pdf
	var _e = reqOptions.e;
	_options.streamType = (!!_e && _options.streamType.indexOf(_e.toLowerCase()) > -1) ? _e.toLowerCase() : _options.streamType[0];

	// UserAgent браузера для снимка
	_options.userAgent = reqOptions.ua || _options.userAgent;


	var ext = _options.streamType;

	var fileName = uuid.v4().replace(/\-/g, "_") + '_' + url_prepare(url) + '.' + ext;
	var filePath = path.join(__dirname, fileName);

	webshot(url, filePath, _options, function(err) {
		if (err) {
			console.log(err);
			return res.status(500).json({
				error: true,
				message: "Screenshot error!"
			});
		}

		/**
		 * Сохраним запись о создании снимка в БД
		 */
		var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.ip;
		if (ip.indexOf("127.0.0.1") > -1) {
			ip = "127.0.0.1";
		} else if (ip.indexOf("::") == 0) {
			ip = ip.split(":")[3];
		}
		Statistic
			.findOne({
				ip: ip,
				url: url
			}, function(err, doc) {
				if (err) {} else if (doc) {
					Statistic
						.update({
							_id: doc._id
						}, {
							$inc: {
								inc_counter: 1
							}
						})
						.exec();
				} else {
					var stats = [{
						url: url,
						ip: ip
					}];
					Statistic.create(stats, function(err, doc) {});
				}
			});

		return res.sendFile(filePath, fileName, function(err) {
			fs.unlink(filePath);
		});
	});

}
