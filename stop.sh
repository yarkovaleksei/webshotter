#!/bin/bash

set -e

pm2 stop app.json && pm2 delete app.json
