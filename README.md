## webshotter

### Сервис создания скриншотов вебстраниц

**Пример ссылки:**

[https://web-screen-shot.ru/screenshot?url=checkitlink.com&sh=all](https://web-screen-shot.ru/screenshot?url=checkitlink.com&sh=all)

- - -
**Можно протестировать сервис так:**
```bash
$ wget "https://web-screen-shot.ru/screenshot?url=checkitlink.com&sh=all" -O screenshot.png
```

- - -
### Параметры:

**url** - url страницы (Единственный обязательный аргумент). *По-умолчанию: null*

**d** - задержка в милисекундах (1000 = 1 секунда) после рендера страницы. Нужен, например, для того, чтобы отработали все скрипты после загрузки. *По-умолчанию: 0*

**ww** - ширина окна браузера (размер указывается в пикселях). *По-умолчанию: 1024*

**wh** - высота окна браузера (размер указывается в пикселях). *По-умолчанию: 768*

**sw** - ширина снимка (размер указывается в пикселях). Может быть равен 'window'. *По-умолчанию: 'window'*

**sh** - высота снимка (размер указывается в пикселях). Может быть равен 'window' или 'all'. *По-умолчанию: 'window'*

**q** - качество картинки (только если параметр 'e' установлен в jpg[jpeg]). *По-умолчанию: 75*

**e** - формат снимка. Может быть равен png, jpg, jpeg или pdf. *По-умолчанию: png*

**ua** - UserAgent браузера для снимка. *По-умолчанию: "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36"*

- - -
### Развертывание проекта

**Сперва установим глобально следующие модули:**

```bash
$ npm install -g pm2 bower gulp
```

**А теперь можно установить проект:**

```bash
$ git clone git@bitbucket.org:yarkovaleksei/webshotter.git
$ cd webshotter
$ chmod +x *.sh
$ npm install
$ bower install --allow-root
$ gulp build
$ ./start.sh
```

- - -
**Протестируем:**

```bash
# Будет отправлено 10 запросов с помощью curl на создание скрина с google.com
# В ответах будет информация о работе модуля cluster
$ ./test.sh
```
