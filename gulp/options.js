var options = {};

/**
 * Общие настройки
 */
// Папка с исходниками
options.src = './src';

// Папка для сборки проекта
options.dest = './public';


/**
 * Настройки для поиска файлов
 */
options.mask = {};

// Маска для всех файлов
options.mask.all_mask = '**/*';
// Маска для JS скриптов
options.mask.js_mask = '**/*.js';
// Маска для минифицированных JS скриптов
options.mask.js_min_mask = '**/*.min.js';
// Маска для CSS стилей
options.mask.css_mask = '**/*.css';
// Маска для минифицированных CSS стилей
options.mask.css_min_mask = '**/*.min.css';
// Маска для исходников LESS
options.mask.less_mask = '**/*.less';
// Маска для исходников LESS, которые не нужно включать в конечный файл
options.mask.less_exclude_mask = '**/includes/*.less';
// Маска для картинок
options.mask.img_mask = '**/*.{png,jpg,gif,jpeg,ico}';
// Маска для исходников JADE
options.mask.jade_mask = '**/*.pug';
// Маска для исходников JADE, для которых не нужно создавать HTML файл
options.mask.jade_exclude_mask = '**/_*.pug';
// Маска для HTML файлов
options.mask.html_mask = '**/*.html';


/**
 * Настройки путей к файлам исходников
 */
options.input = {};

// Каталог JS скриптов
options.input.jsdir = [options.src, 'js'].join('/');
// Каталог LESS стилей
options.input.cssdir = [options.src, 'less'].join('/');
// Каталог JADE шаблонов
options.input.jadedir = [options.src, 'pug'].join('/');
// Каталог изображений
options.input.imgdir = [options.src, 'img'].join('/');
// Имя главного файла LESS, который нужно компилировать в CSS
options.input.lessMainFile = [options.input.cssdir, 'style.less'].join('/');
// Имя главного файла JS, в который надо конкатенировать все скрипты
options.input.jsMainFile = 'script.js';
// Имя главного файла CSS, в который надо конкатенировать все стили
options.input.cssMainFile = 'style.css';
// Путь к файлу с переменными для компиляции JADE
options.input.jadevars = [options.input.jadedir, 'vars.json'].join('/');


/**
 * Настройки путей к файлам собранного проекта
 */
options.output = {};

// Путь к каталогу для скомпилированных HTML файлов
options.output.jadedir = options.dest;
// Путь к каталогу для картинок
options.output.imgdir = [options.dest, 'img'].join('/');
// Путь к каталогу для скриптов
options.output.jsdir = [options.dest, 'js'].join('/');
// Путь к каталогу для стилей
options.output.cssdir = [options.dest, 'css'].join('/');
// Имя главного файла JS, который содержит код приложения
options.output.jsMainFile = 'script.min.js';
// Имя главного файла CSS, который содержит таблицы стилей приложения
options.output.cssMainFile = 'style.min.css';




module.exports = options;
