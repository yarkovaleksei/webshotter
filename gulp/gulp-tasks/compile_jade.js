module.exports = function (gulp, plugins, options) {
	return function () {
		// Компилирует файлы *.jade в *.html 
		// и отправляет в выходную папку
		gulp.task('compile_jade', function () {
			var path = require('path');
			var JADE_VARS = require(path.join(process.cwd(), options.input.jadevars));
			gulp.src([
					[options.input.jadedir, options.mask.jade_mask].join('/'),
					['!' + options.input.jadedir, options.mask.jade_exclude_mask].join('/')
				], {cwd: process.cwd()})
				.pipe(plugins.data(function(file) {
					return JADE_VARS;
				}))
				.pipe(plugins.jade({
					pretty: true
				}))
				.on('error', console.log)
				.pipe(plugins.html_prettify({
					indent_char: '\t', 
					indent_size: 1
				}))
				.on('error', console.log)
				.pipe(gulp.dest(options.output.jadedir, {cwd: process.cwd()}));
		});
	};
};
