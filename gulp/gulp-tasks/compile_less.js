module.exports = function (gulp, plugins, options) {
    return function () {
		// Компилирует *.less в *.css в папке разработки 
		// и отправляет в выходную папку
		gulp.task('compile_less', function () {
			var path = require('path');
			gulp.src([
					[options.input.cssdir, options.mask.css_mask].join('/'),
					[options.input.cssdir, options.mask.less_mask].join('/'),
					['!' + options.input.cssdir, options.mask.less_exclude_mask].join('/')
				], {cwd: process.cwd()})
				.pipe(plugins.less())
				.on('error', console.log)
				.pipe(plugins.autoprefixer({
					cascad: true,
					browsers: ['last 2 versions']
				}))
				.on('error', console.log)
				.pipe(plugins.concat(options.input.cssMainFile))
				.on('error', console.log)
				.pipe(gulp.dest(options.output.cssdir, {cwd: process.cwd()}))
				.on('error', console.log)
				.pipe(plugins.csso())
				.on('error', console.log)
				.pipe(plugins.rename(options.output.cssMainFile))
				.on('error', console.log)
				.pipe(gulp.dest(options.output.cssdir, {cwd: process.cwd()}));
		});
	};
};
