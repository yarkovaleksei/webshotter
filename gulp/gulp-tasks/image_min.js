module.exports = function (gulp, plugins, options) {
    return function () {
		// Обрабатывает и сжимает картинки в папке разработки 
		// и отправляет в выходную папку
		gulp.task('image_min', function () {
			return gulp.src([options.input.imgdir, options.mask.img_mask].join('/'), {cwd: process.cwd()})
				.pipe(plugins.imagemin())
				.on('error', console.log)
				.pipe(gulp.dest(options.output.imgdir, {cwd: process.cwd()}))
		});
    };
};
