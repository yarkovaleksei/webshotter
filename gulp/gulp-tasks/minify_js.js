module.exports = function (gulp, plugins, options) {
    return function () {
		// Объединяет и сжимает *.js скрипты в папке разработки 
		// и отправляет в выходную папку
		gulp.task('minify_js', function () {
			gulp.src([
			         	// ищем остальные скрипты во всех вложенных папках
						[options.input.jsdir, options.mask.js_mask].join('/'),
						// исключаем сжатые скрипты
						['!' + options.input.jsdir, options.mask.js_min_mask].join('/')
				], {cwd: process.cwd()})
				// объединяем в один файл
				.pipe(plugins.concat(options.input.jsMainFile))
				.on('error', console.log)
				.pipe(gulp.dest(options.output.jsdir, {cwd: process.cwd()}))
				.on('error', console.log)
				// сжимаем файл
				.pipe(plugins.uglify({
					mangle: true,
					unused: true,
					showStack: true
				}))
				.on('error', console.log)
				.pipe(plugins.rename(options.output.jsMainFile))
				.on('error', console.log)
				// отправляем в выходную папку
				.pipe(gulp.dest(options.output.jsdir, {cwd: process.cwd()}));
		});
    };
};
